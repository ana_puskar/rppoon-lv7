using System;
using System.Collections.Generic;
using System.Text;

namespace RPPOON_LV7_AP
{
    class LinearSearch : SearchStrategy
    {
        int arrayPlacement = 0;
        public override void Search(double[] array, int searchedNumber)
        {
            
            for(int i = 0; i < array.Length; i++)
            {
                if (array[i] == searchedNumber)
                {
                    arrayPlacement =  i + 1;
                } 
            }
            if(arrayPlacement != 0)
            {
                Console.WriteLine("The number you searched for has been found on the " + this.arrayPlacement + " place of this array.");
            }
            else
            {
                Console.WriteLine("The number you searched for has not been found in this array.");
            }
            
        }
    }
}