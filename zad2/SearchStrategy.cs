using System;
using System.Collections.Generic;
using System.Text;

namespace RPPOON_LV7_AP
{
    abstract class SearchStrategy
    {
        public abstract void Search(double[] array, int searchedNumber);
    }
}
