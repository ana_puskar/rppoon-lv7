using System;

namespace RPPOON_LV7_AP
{
    class Program
    {
        static void Main(string[] args)
        {
            double[] array = new double[] { 1, 12, 8, 26, 84, 3, 56, 78, 21, 32 };

            NumberSequence numberSequence = new NumberSequence(array);
            Console.WriteLine("Which number are you looking for?");
            int searchedNumber = Int32.Parse(Console.ReadLine());
            LinearSearch linearSearch = new LinearSearch();
            numberSequence.setSearchStrategy(linearSearch, searchedNumber);
            numberSequence.Search();
        }
    }
}
