using System;

namespace RPPOON_LV7_AP
{
    class Program
    {
        static void Main(string[] args)
        {
            double[] array = new double[] { 1, 12, 8, 26, 84, 3, 56, 78, 21, 32 };

            NumberSequence numberSequence = new NumberSequence(array);
            Console.WriteLine("We need to sort this:\n" + numberSequence.ToString());
            BubbleSort bubbleSort = new BubbleSort();
            numberSequence.SetSortStrategy(bubbleSort);
            numberSequence.Sort();
            Console.WriteLine("Bubble sort:\n" + numberSequence.ToString());

            numberSequence = new NumberSequence(array);
            SequentialSort sequentialSort = new SequentialSort();
            numberSequence.SetSortStrategy(sequentialSort);
            numberSequence.Sort();
            Console.WriteLine("Sequential sort:\n" + numberSequence.ToString());

            numberSequence = new NumberSequence(array);
            CombSort combSort = new CombSort();
            numberSequence.SetSortStrategy(combSort);
            numberSequence.Sort();
            Console.WriteLine("Combination sort:\n" + numberSequence.ToString());
        }
    }
}
