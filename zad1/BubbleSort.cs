using System;
using System.Collections.Generic;
using System.Text;

namespace RPPOON_LV7_AP
{
    class BubbleSort : SortStrategy
    {
        public override void Sort(double[] array)
        {
            int arraySize = array.Length;
            for(int i = 0; i < arraySize; i++)
            {
                for(int j = 0; j < arraySize - 1 - i; j++)
                {
                    if(array[j + 1] < array[j])
                    {
                        Swap(ref array[j], ref array[j + 1]);
                    }
                }
            }
        }

    }
}