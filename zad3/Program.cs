using System;

namespace RPPOON___LV7___AP__2
{
    class Program
    {
        static void Main(string[] args)
        {
            BuyVisitor visitor = new BuyVisitor();

            DVD dvd = new DVD("Barbie Fairytopia", DVDType.MOVIE, 15.22);
            VHS vhs = new VHS("Life: the movie", 12.56);
            Book book = new Book("Shadows of the land", 17.46);

            Console.WriteLine("Item you're looking at: " + dvd.ToString() + "\nwith tax: " + dvd.Accept(visitor));
            Console.WriteLine("Item you're looking at: " + vhs.ToString() + "\nwith tax: " + vhs.Accept(visitor));
            Console.WriteLine("Item you're looking at: " + book.ToString() + "\nwith tax: " + book.Accept(visitor));
        }
    }
}